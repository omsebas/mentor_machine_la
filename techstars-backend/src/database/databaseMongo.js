const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/backup_meetings', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const mongoDB = mongoose.connection;

module.exports = mongoDB;
