const Schedule = require('../models/Schedule');
const Blocks = require('../models/Blocks');
const Slots = require('../models/Slots');
const Days = require('../models/Days');
const Companies = require('../models/Companies');
const Mentors = require('../models/Mentors');
const Company_surveys = require('../models/Company_survey');
const Mentor_Surveys = require('../models/Mentor_survey');
const BackupMeetings = require('../models/BackupMeetings');

const { Op } = require('sequelize');

async function saveProgram(req, res) {
  // finding mentors that has not been schedule yet (haven't chosen day and block but already have companies assigned)
  try {
    programBackupJson = [];

    //Retrieve data from tables
    const allSchedule = await Schedule.findAll({ raw: true });
    const allblocks = await Blocks.findAll({ raw: true });
    const allslots = await Slots.findAll({ raw: true });
    const alldays = await Days.findAll({ raw: true });
    const allcompanies = await Companies.findAll({ raw: true });
    const allmentors = await Mentors.findAll({ raw: true });
    const allcompaniesSurveys = await Company_surveys.findAll({ raw: true });
    const allmentorSurveys = await Mentor_Surveys.findAll({ raw: true });

    //save data to mongodb
    const small = new BackupMeetings({
      schedule: allSchedule,
      blocks: allblocks,
      slots: allslots,
      days: alldays,
      companies: allcompanies,
      mentors: allmentors,
      companySurveys: allcompaniesSurveys,
      mentorSurveys: allmentorSurveys,
    });
    await small.save(function (err) {
      if (err) return handleError(err);
      // saved!
    });
    res.json({ status: 'success' });
  } catch (error) {
    console.log(error);
  }
}
async function getProgram(req, res) {
  // finding mentors that has not been schedule yet (haven't chosen day and block but already have companies assigned)
  try {
    const backup = await BackupMeetings.find({});
    res.json(backup);
  } catch (error) {
    console.log(error);
  }
}

module.exports = { getProgram, saveProgram };
