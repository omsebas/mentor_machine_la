const schedule = require('../models/Schedule');
const companies = require('../models/Companies');
const days = require('../models/Days');
const blocks = require('../models/Blocks');
const slots = require('../models/Slots');
const mentors = require('../models/Mentors');
const mentor_survey = require('../models/Mentor_survey');
const company_survey = require('../models/Company_survey');
const { spawn } = require('child_process');
const { Op, QueryTypes } = require('sequelize');
const sequelize = require('../database/database');

async function downloadFile(req, res) {
  const objects = await companies.findAll({
    attributes: ['company_id', 'company', 'email'],
  });

  const companySchedule = await sequelize.query(
    'SELECT companies.company, mentors.mentor, slots.slot, days.day, blocks.block\
    FROM schedule\
    INNER JOIN mentors\
    ON schedule.mentor_id = mentors.mentor_id\
    INNER JOIN days\
    ON schedule.day_id = days.day_id\
    INNER JOIN slots\
    ON schedule.slot_id = slots.slot_id\
    INNER JOIN companies\
    ON schedule.company_id = companies.company_id\
    INNER JOIN blocks\
    ON schedule.block_id = blocks.block_id;',
    {
      type: QueryTypes.SELECT,
    }
  );
  console.log(companySchedule);
  res.json(companySchedule);
}

module.exports = { downloadFile };
