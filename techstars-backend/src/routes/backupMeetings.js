const { Router } = require('express');
const {
  getProgram,
  saveProgram,
} = require('../controllers/backupMeetings.controller');
const router = Router();

//Company data
//router.post("/", createCompany);
router.get('/', getProgram);
router.get('/save', saveProgram);
//Company data by id
/* router.get('/:id', getCompanyById);
router.delete('/:id', deleteCompany);
router.put('/:id', updateCompany); */

module.exports = router;
