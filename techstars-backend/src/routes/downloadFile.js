const { Router } = require('express');
const { downloadFile } = require('../controllers/downloadFile.controller');
const router = Router();

router.post('/', downloadFile);

module.exports = router;
