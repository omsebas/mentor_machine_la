// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
const BackupMeetingsSquema = new Schema({
  schedule: [
    {
      meet_id: String,
      mentor_id: String,
      day_id: String,
      block_id: String,
      company_id: String,
      slot_id: String,
      created_at: String,
      updated_at: String,
    },
  ],
  blocks: [
    {
      block_id: String,
      block: String,
    },
  ],
  slots: [
    {
      slot_id: String,
      slot: String,
    },
  ],
  days: [
    {
      day_id: String,
      day: String,
    },
  ],
  companies: [
    {
      company_id: String,
      company: String,
      email: String,
    },
  ],
  mentors: [
    {
      mentor_id: String,
      mentor: String,
      email: String,
    },
  ],
  mentor_survey: [
    {
      survey_id: String,
      vote: Number,
      feedback: String,
      ranking: Number,
      mentor_id: String,
      company_id: String,
    },
  ],
  company_survey: [
    {
      survey_id: String,
      vote: Number,
      feedback: String,
      ranking: Number,
      mentor_id: String,
      company_id: String,
    },
  ],
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('BackupMeetings', BackupMeetingsSquema);
