const app = require('./app');
const { sendMails } = require('./mail/send_mails');

const port = process.env.PORT || 5000;
function main() {
  app.listen(port, () => console.log(`Server listening in port ${port}`));
  sendMails();
}
main();
/*
// Utilizar funcionalidades del Ecmascript 6
('use strict');
// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
var mongoose = require('mongoose');
// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;
// Usamos el método connect para conectarnos a nuestra base de datos
mongoose
  .connect('mongodb://localhost:27017/backup_meetings', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    // Cuando se realiza la conexión, lanzamos este mensaje por consola
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    console.log(
      'La conexión a la base de datos curso_mean_social se ha realizado correctamente'
    );
    const port = process.env.PORT || 5000;
    // CREAR EL SERVIDOR WEB CON NODEJS
    app.listen(port, () => {
      console.log('servidor corriendo en http://localhost:5000');
      sendMails();
    });
  })
  // Si no se conecta correctamente escupimos el error
  .catch((err) => console.log(err)); */
