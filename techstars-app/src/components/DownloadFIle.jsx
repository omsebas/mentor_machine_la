import React, { useState } from 'react';
import { Form, Select, Button, Radio } from 'antd';
import { CSVDownloader, jsonToCSV } from 'react-papaparse';
import * as aq from 'arquero';

const DownloadFile = ({ resSchedule }) => {
  const [companySchedule, setCompanySchedule] = useState(resSchedule);
  const [form] = Form.useForm();

  function downloadCSV(datajson, filenametext) {
    let data, filename, link;
    let csv = convertArrayOfObjectsToCSV(datajson, null, null);
    if (csv == null) return;

    filename = filenametext || 'companySchedule.csv';

    if (!csv.match(/^data:text\/csv/i)) {
      csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
  }

  function convertArrayOfObjectsToCSV(
    datajson,
    columnDelimiter,
    lineDelimiter
  ) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = datajson || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = columnDelimiter || ',';
    lineDelimiter = lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function (item) {
      ctr = 0;
      keys.forEach(function (key) {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }

  const submitQuery = (value, resSchedule) => {
    form.resetFields();
    if (value.download === 'companySchedule') {
      console.log('fetching');
      fetch(
        'https://mentor-machine-api.herokuapp.com/api/downloadFile' /* Route to send the CSV file to 
                                                    generate the schedule */,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          body: null,
        }
      )
        .then((response) => response.json())
        .then((result) => {
          /*    result = aq
            .from(result)
            .derive('mentor', 'day', 'block')
            .pivot('slot', 'company')
            .rename({ day: 'start date', day: 'Day', block: 'Block' })

            .objects(); */
          console.log(result);
          result = result.map((obj) => {
            let today = new Date(obj.day + 'T' + obj.slot);
            let year = today.getFullYear();
            let mes = today.getMonth() + 1;
            let dia = today.getDate() + 1;
            //let d = new Date(obj.slot);
            let h = today.getHours();
            let m = today.getMinutes();
            let s = today.getSeconds();
            let smore = new Date();
            smore.setTime(today.getTime() + 20 * 60 * 1000);
            let smore1 = smore.getMinutes();
            let hmore = smore.getHours();
            obj['start date'] = dia + '/' + ('0' + mes).slice(-2) + '/' + year;
            obj['end date'] = dia + '/' + ('0' + mes).slice(-2) + '/' + year;
            obj['year'] = year;
            obj['month'] = mes;
            obj['days'] = dia;
            obj['start time'] =
              h + ':' + ('0' + m).slice(-2) + ':' + ('0' + s).slice(-2);
            obj['end time'] =
              hmore +
              ':' +
              ('0' + smore1).slice(-2) +
              ':' +
              ('0' + s).slice(-2);
            obj['All Day Event'] = 'FALSE';
            return obj;
          });
          let resultCSV = aq
            .from(result)
            .select(
              'company',
              'mentor',
              'start date',
              'All Day Event',
              'start time',
              'end date',
              'end time'
            )
            .rename({ mentor: 'subject' })
            .objects();
          downloadCSV(resultCSV, null);
        })
        .catch((error) => {
          alert('Error:', error);
        });
    } else if (value === 'mentorSchedule') {
      downloadCSV(companySchedule, null);
    } else {
      alert('Select an option');
    }
  };

  return (
    <>
      <Form
        layout={'inline'}
        form={form}
        initialValues={{
          layout: 'inline',
        }}
        onFinish={submitQuery}
      >
        <Form.Item name='download'>
          <Select
            placeholder={'Download schedule'}
            style={{ width: 200 }}
            allowClear={true}
          >
            <Select.Option value='mentorSchedule'>MentorSchedule</Select.Option>
            <Select.Option value='companySchedule'>
              CompanySchedule
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type='primary' htmlType='submit'>
            Download
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default DownloadFile;
